I needed to make Iframes. In particular, I needed to be able to slap Iframes
into random parts of a Panels page, and render Link fields as Iframes rather
than clickable links. And so, Frameup module was born. It allows you to set all
of the HTML5 Iframe attributes like "seamless," even though current browsers
don't support them. That's okay, though, because the future will arrive
eventually.

Do you need to do things with Iframes, too? Are you annoyed that something in
this module doesn't work the way you'd like it to?

Congratulations! You just volunteered to be co-maintainer, my friend. The
authorities be in touch with you shortly. ;-)