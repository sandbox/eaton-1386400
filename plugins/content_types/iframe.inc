<?php

/**
 * @file
 * "Iframe" content type. It just lets admins jam in a URL and assorted Iframe
 * settings.
 */



/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Iframe embed'),
  'description' => t('Embeds the contents of another URL into the current document using an Iframe.'),

  'single' => TRUE,
  'content_types' => array('iframe'),
  'render callback' => 'frameup_iframe_render',
  'defaults' => array(),

  'edit form' => 'frameup_iframe_edit_form',

  // Icon goes in the directory with the content type.
  'icon' => 'icon_example.png',
  'category' => array(t('Embeds')),
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function frameup_iframe_render($subtype, $conf, $args, $context) {
  $block = new stdClass();

  // The title actually used in rendering
  $block->content = '';

  $element = array(
    '#type' => 'html_tag',
    '#tag' => 'iframe',
  );

  $attributes = array('src' => $conf['src']);
  
  foreach (array('height', 'width', 'srcdoc') as $attribute) {
    if (!empty($conf[$attribute])) {
      $attributes[$attribute] = $conf[$attribute];
    }
  }
  if ($conf['seamless']) {
    $attributes['seamless'] = 'seamless';
  }
  if ($conf['sandbox']) {
    $attributes['seamless'] = $conf['sandbox_options'];
  }
  
  $element['#attributes'] = $attributes;
  
  $block->content = drupal_render($element);

  return $block;
}

/**
 * 'Edit form' callback for the content type.
 */
function frameup_iframe_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form += _frameup_settings_form($conf);
  return $form;
}

function frameup_iframe_edit_form_submit(&$form, &$form_state) {
  foreach (array('src', 'srcdoc', 'width', 'height', 'seamless', 'sandbox', 'sandbox_options') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}
